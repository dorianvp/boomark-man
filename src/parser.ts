import sqlite3 from 'sqlite3';
import { open } from 'sqlite';
import { readFileSync } from 'fs';
import { error } from 'console';

/**
 * @description Parses data from chromium-based browsers 
 */
export function parseChromium(path: string) {
	const data = JSON.parse(readFileSync(path, 'utf-8'));

	// Traverse json object and push items to new array
	const urls = [..._getUrls(data.roots.bookmark_bar), ..._getUrls(data.roots.other), ..._getUrls(data.roots.synced)];

	return urls;
}

/**
 * @description Parses data from firefox-based browsers 
 */
export async function parseFirefox(path: string) {
	const urls: string[] = [];

	try {
		const db = await open({
			filename: path,
			driver: sqlite3.Database
		});
		const result = await db.all('SELECT DISTINCT url FROM moz_bookmarks b INNER JOIN moz_places o WHERE b.fk = o.id');

		result.forEach(item => {
			urls.push(item.url);
		});

		await db.close();
	} catch (err) {
		error(err);
	}

	return urls;
}

function _getUrls(_object: { type: string; children: any[]; url: any; }): any {
	if (_object.type === 'folder') {
		return [].concat(..._object.children.map(item => _getUrls(item)));
	} else if (_object.type === 'url') {
		return _object.url;
	} else {
		return [];
	}
}