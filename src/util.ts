import { homedir } from 'os';
import { existsSync, readFileSync, readdirSync } from 'fs';
import { join } from 'path';
import ini from 'ini';

/**
 * Returns an array containing browser paths.
 */
export function getBrowserPaths() {
	const FIREFOX = homedir() + '/.mozilla/firefox/';
	const LIBREWOLF = homedir() + '/.librewolf/';
	const CHROMIUM = homedir() + '/.config/chromium/';
	const CHROME = homedir() + '/.config/google-chrome/';
	const VIVALDI = homedir() + '/.config/vivaldi/';
	const BRAVE = homedir() + '/.config/BraveSoftware/Brave-Browser/';

	const potentialPaths = [FIREFOX, LIBREWOLF, CHROME, CHROMIUM, BRAVE];
	const paths: string[] = [];

	potentialPaths.forEach(item => {
		_checkPathExistence(item, paths);
	});

	return paths;
}

function _checkPathExistence(path: string, array: string[]) {
	if (existsSync(path)) {
		array.push(path);
	}
}

/**
 * Returns an array containing paths to each profile's bookmark file
 * @param {string[]} browserPaths A list of paths from where to extract profile bookmark paths
 */
export function getProfileBookmarkPaths(browserPaths: string[]) {
	return browserPaths.map(_path => {
		const paths = [];

		if (existsSync(join(_path, 'profiles.ini'))) {
			// Firefox-based browser
			const config = ini.parse(readFileSync(join(_path, 'profiles.ini'), 'utf-8'));
			const profileCount = Object.keys(config).filter(item => item.includes('Profile')).length;

			for (let i = 0; i < profileCount; i++) {
				const element = config[`Profile${i}`];

				// Check if Path property starts with / or is relative to current directory
				if (element.Path.startsWith('/')) {
					paths.push(join(element.Path, 'places.sqlite'));
				} else {
					paths.push(join(_path, element.Path, 'places.sqlite'));
				}
			}

		} else {
			// Chromium-based
			const potentialPaths = _getDirectories(_path);
			potentialPaths.push('Default');

			potentialPaths.map(_potentialPath => {
				if (existsSync(join(_path, _potentialPath, 'Bookmarks'))) {
					paths.push(join(_path, _potentialPath, 'Bookmarks'));
				}
			});

		}

		return paths;
	});


	function _getDirectories(source: string) {
		return readdirSync(source, { withFileTypes: true })
			.filter(dirent => dirent.isDirectory() && dirent.name.includes('Profile'))
			.map(dirent => dirent.name);
	}

}