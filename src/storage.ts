import { appendFileSync, openSync, readFileSync, readSync, statSync, writeFileSync } from 'fs';
import { join } from 'path';

export async function filterData(newData: string[]) {
	const previousBookmarks = readFileSync(join(process.cwd(), './data/bookmarks.txt'), 'utf-8');
	const previousData = new Map();

	previousBookmarks.split('\n').forEach(line => {
		previousData.set(line, true);
	});

	const filteredData = newData.filter(item => {
		return !previousData.has(item);
	});

	return filteredData;
}

export function saveBookmarks(data: string[]) {
	const newBookmarks = data.join('\n');
	writeFileSync(join(process.cwd(), './data/bookmarks.txt'), newBookmarks);
}

export function appendBookmarks(_bookmarks: string[]) {
	let newBookmarks = _bookmarks.map((item, index) => {
		if (index === _bookmarks.length - 1) return item;
		return item.concat('\n');
	}).join('');

	// Check file size and linebreak
	const fd = openSync(join(process.cwd(), './data/bookmarks.txt'), 'r');

	const buf = Buffer.alloc(1);

	const size = statSync(join(process.cwd(), './data/bookmarks.txt')).size;

	if (statSync(join(process.cwd(), './data/bookmarks.txt')).size !== 0) {
		readSync(fd, buf, 0, 1, size - 1);
		if (buf.toString() !== '\n') {
			newBookmarks = '\n'.concat(newBookmarks);
		}
	}

	appendFileSync(join(process.cwd(), './data/bookmarks.txt'), newBookmarks);
}

/**
 * @param {string[][]} bookmarkFiles 
 */
export function saveBookmarkLocations(bookmarkFiles: string[][]) {
	const potentialLocations = bookmarkFiles.flat();

	const previousLocations = readFileSync(join(process.cwd(), './config/locations'), 'utf-8');
	const previousData = new Map();

	previousLocations.split('\n').forEach(line => {
		previousData.set(line, true);
	});

	const filteredLocations = potentialLocations.filter(item => {
		return !previousData.has(item);
	});

	if (filteredLocations.length > 0) {


		let newLocations = filteredLocations.map((item, index) => {
			if (index === filteredLocations.length - 1) return item;
			return item.concat('\n');
		}).join('');

		// Check file size and linebreak
		const fd = openSync(join(process.cwd(), './config/locations'), 'r');

		const buf = Buffer.alloc(1);

		const size = statSync(join(process.cwd(), './config/locations')).size;

		if (statSync(join(process.cwd(), './config/locations')).size !== 0) {
			readSync(fd, buf, 0, 1, size - 1);
			if (buf.toString() !== '\n') {
				newLocations = '\n'.concat(newLocations);
			}
		}

		appendFileSync(join(process.cwd(), './config/locations'), newLocations);
	}
}

export function getLocations() {
	return readFileSync(join(process.cwd(), './config/locations'), 'utf-8').split('\n');
}