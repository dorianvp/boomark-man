import chokidar from 'chokidar';
import { copyFileSync, unlink } from 'fs';
import { extname, join } from 'path';
import { parseChromium, parseFirefox } from './src/parser.ts';
import { appendBookmarks, filterData, getLocations, saveBookmarkLocations } from './src/storage.ts';
import { getBrowserPaths, getProfileBookmarkPaths } from './src/util.ts';

// Check which web browsers the user has installed, read profiles and save each location into a file
const BROWSERS = getBrowserPaths();
const PATHS = getProfileBookmarkPaths(BROWSERS);

saveBookmarkLocations(PATHS);
const LOCATIONS = getLocations();

const watcher = chokidar.watch(LOCATIONS, {
	persistent: true,
	cwd: '.'
});

watcher
	.on('change', async (path) => {
		const tempFile = `./data/temp/${Date.now()}${extname(path) ? extname(path) : ''}`;

		// Add to temp folder
		copyFileSync(path, join(process.cwd(), tempFile));

		let data;
		if (extname(path) == '.sqlite') {
			data = await parseFirefox(tempFile);
		} else {
			data = parseChromium(tempFile);
		}

		let updatedBookmarks = [];
		updatedBookmarks = await filterData(data);

		// Store
		if (updatedBookmarks.length > 0) {
			appendBookmarks(updatedBookmarks);
		}

		unlink(join(process.cwd(), tempFile), (err) => {
			if (err) throw err;
		});

	})
	.on('error', error => console.log(`Watcher error: ${error}`));


function signalHandler() {
	watcher.close().then(() => {
		process.exit();
	});
}

process.on('SIGINT', signalHandler);
process.on('SIGTERM', signalHandler);
process.on('SIGQUIT', signalHandler);
